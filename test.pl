#!/usr/bin/perl

use v5.10.0;
use warnings;
use strict;

use JSON;
use HTTP::Request;
use LWP::UserAgent;

my %data = (
    'perf_id' => 123,
    'name'    => 'test',
    'price'   => 999.99,
    'amount'  => 15
);

my $json = encode_json \%data;

#print "$json\n";
my $req = HTTP::Request->new( 'POST', 'http://172.23.0.2/order/0987' );
$req->header( 'Content-Type' => 'application/json' );
$req->content($json);

my $lwp      = LWP::UserAgent->new;
my $response = $lwp->request($req);
print $response->content, "\n";

