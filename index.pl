#!/usr/bin/env perl

use v5.10.0;
use warnings;
use strict;

use File::Basename;
use Beam::Wire;
use Data::Dumper;
use CGI;
use URI;
use JSON;

use lib dirname(__FILE__) . '/src';

use Model::User;
use Controller::Order;

print "Content-type: text/html\n\n";

my $wire = Beam::Wire->new( file => dirname(__FILE__) . '/config/config.yaml' );

#@type Service::Router
my $router = $wire->get('service_router');

$router->add( 'POST', '/order/(\d+)', 'Controller::Order', 'update' );
my $result = $router->match();
unless ($result) {
    print('{"code":404}');
    exit(0);
}

my $controller = $$result{'controller'};
my $action     = $$result{'action'};
my @params     = ();
for my $inject ( $controller->inject() ) {
    my $tmp;
    eval { $tmp = $wire->get($inject); };
    push( @params, $tmp );
}

my $orderController = $controller->new(@params);
my (%r) = $orderController->$action( @{ $$result{'result'} } );
$wire->get('dbh')->disconnect();
print( encode_json \%r );

