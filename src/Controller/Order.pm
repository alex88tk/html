package Controller::Order;

use warnings;
use strict;
use Model::Action;
use JSON;
use Data::Dumper;
use Scalar::Util::Numeric qw(isint isfloat);

sub inject {
    return ( 'cgi', 'model_order', 'model_user', 'model_action',
        'service_journal' );
}

sub new {
    my $class = shift;
    return bless(
        {
            #@type CGI
            _cgi => shift,

            #@type Model::Order
            _model_order => shift,

            #@type Model::User
            _model_user => shift,

            #@type Model::Action
            _model_action => shift,

            #@type Service::Journal
            _service_journal => shift
        },
        $class
    );
}

sub update {
    my ( $self, $number ) = @_;

    my $data = decode_json $self->{_cgi}->param('POSTDATA');

    if (   !defined( $$data{'perf_id'} )
        || !isint( $$data{'perf_id'} )
        || $$data{'perf_id'} < 1 )
    {
        return ( 'error' => 'perf_id must be int AND > 0' );
    }

    if ( !defined( $$data{'name'} ) || length( $$data{'name'} ) == 0 ) {
        return ( 'error' => 'name must be init' );
    }

    if (   !defined( $$data{'price'} )
        || !( isfloat( $$data{'price'} ) || isint( $$data{'price'} ) )
        || $$data{'price'} <= 0 )
    {
        return ( 'error' => 'price must be float AND > 0' );
    }

    if (   !defined( $$data{'amount'} )
        || !isint( $$data{'amount'} )
        || $$data{'amount'} < 1 )
    {
        return ( 'error' => 'amount must be int AND > 0' );
    }

    if ( !defined($number) || !isint($number) || $number < 1 ) {
        return ( 'error' => 'order number must be int AND > 0' );
    }

    my ( $order_number, $perf_id, $name, $price, $amount ) = (
        $number,         $$data{'perf_id'}, $$data{'name'},
        $$data{'price'}, $$data{'amount'}
    );

    #@type Model::Order
    my $modelOrder = $self->{_model_order};
    my $order      = $modelOrder->searchNumber($order_number);
    unless ($order) {
        $order = Entity::Order->new();
        $order->setNumber($order_number)->setPerfId($perf_id)->setPrice($price)
          ->setAmount($amount)->setId( $modelOrder->save($order) );

        $self->{_model_action}->save(
            Entity::Action->new(
                'create new order with number: ' . $order->getNumber,
                $order->getId
            )
        );

    }
    else {
        $order->setNumber($order_number)->setPerfId($perf_id)->setPrice($price)
          ->setAmount($amount);
        $modelOrder->update($order);

        $self->{_model_action}->save(
            Entity::Action->new(
                'update new order with number: ' . $order->getNumber,
                $order->getId
            )
        );
    }

    #@type Model::User
    my $modelUser = $self->{_model_user};

    #@type Entity::User
    my $user = $modelUser->searchName($name);
    unless ($user) {
        $user = Entity::User->new();
        $user->setName($name)->setId( $modelUser->save($user) );

        $self->{_model_action}->save(
            Entity::Action->new(
                'add new user:' . $user->getName,
                $order->getId
            )
        );
    }

    my %newBody = (
        'perf_id'   => $order->getPerfId(),
        'name'      => $user->getName(),
        'client_id' => $user->getId(),
        'price'     => $order->getPrice(),
        'amount'    => $order->getAmount(),
        'order_id'  => $order->getId(),
    );

    #@type Service::Journal
    $self->{'_service_journal'}->save(%newBody);

    return ( 'status' => 1 );
}

1;
