package Entity::Order;

use warnings;
use strict;

sub new {
    my $class = shift;
    return bless(
        {
            _id      => 0,
            _number  => '',
            _perf_id => '',
            _price   => '',
            _amount  => '',
        },
        $class
    );
}

sub setId {
    my ( $self, $id ) = @_;
    $self->{_id} = $id;
    return $self;
}

sub getId() {
    my ($self) = @_;
    return $self->{_id};
}

sub setNumber {
    my ( $self, $number ) = @_;
    $self->{_number} = $number;
    return $self;
}

sub getNumber {
    my ($self) = @_;
    return $self->{_number};
}

sub setPerfId {
    my ( $self, $perfId ) = @_;
    $self->{_perf_id} = $perfId;
    return $self;
}

sub getPerfId {
    my ($self) = @_;
    return $self->{_perf_id};
}

sub setPrice {
    my ( $self, $price ) = @_;
    $self->{_price} = $price;
    return $self;
}

sub getPrice {
    my ($self) = @_;
    return $self->{_price};
}

sub setAmount {
    my ( $self, $amount ) = @_;
    $self->{_amount} = $amount;
    return $self;
}

sub getAmount {
    my ($self) = @_;
    return $self->{_amount};
}

1;
