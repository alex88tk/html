package Entity::Action;

use warnings;
use strict;

sub new {
    my $class = shift;
    return bless(
        {
            _id       => 0,
            _info     => shift,
            _time     => '',
            _order_id => shift,
        },
        $class
    );
}

sub setId {
    my ( $self, $id ) = @_;
    $self->{_id} = $id;
    return $self;
}

sub getId() {
    my ($self) = @_;
    return $self->{_id};
}

sub setInfo {
    my ( $self, $name ) = @_;
    $self->{_info} = $name;
}

sub getInfo () {
    my ($self) = @_;
    return $self->{_info};
}

sub setOrderId {
    my ( $self, $name ) = @_;
    $self->{_order_id} = $name;
}

sub getOrderId () {
    my ($self) = @_;
    return $self->{_order_id};
}

1;
