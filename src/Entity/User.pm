package Entity::User;

use warnings;
use strict;

sub new {
    my $class = shift;
    return bless(
        {
            _id   => 0,
            _name => '',
        },
        $class
    );
}

sub setId {
    my ( $self, $id ) = @_;
    $self->{_id} = $id;
    return $self;
}

sub getId() {
    my ($self) = @_;
    return $self->{_id};
}

sub setName {
    my ( $self, $name ) = @_;
    $self->{_name} = $name;
    return $self;
}

sub getName {
    my ($self) = @_;
    return $self->{_name};
}

1;
