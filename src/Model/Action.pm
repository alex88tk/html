package Model::Action;

use warnings;
use strict;
use DBI;
use Entity::Action;

sub new {
    my $class = shift;
    return bless(
        {
            #@type DBI
            _dbh => shift
        },
        $class
    );
}

sub save {
    my (
        $self,

        #@type Entity::Action
        $action
    ) = @_;

    my $sth = $self->{_dbh}->prepare(
        'INSERT INTO actions(info, time, order_id) VALUES (?, NOW(), ?)');
    $sth->execute( $action->getInfo(), $action->getOrderId() );
    return $self->{_dbh}->last_insert_id();
}

1;
