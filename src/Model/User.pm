package Model::User;

use warnings;
use strict;
use DBI;
use Data::Dumper;
use Entity::User;

sub new {
    my $class = shift;
    return bless(
        {
            #@type DBI
            _dbh => shift
        },
        $class
    );
}

#@returns Entity::User,boolse
sub searchName {
    my ( $self, $name ) = @_;
    my $sth = $self->{_dbh}
      ->prepare('Select id, name FROM users WHERE name = ? LIMIT 1');
    $sth->execute($name);

    if ( my $item = $sth->fetchrow_hashref() ) {
        my $user = Entity::User->new();
        $user->setId( $item->{'id'} );
        $user->setName( $item->{'name'} );
        return $user;
    }
    return undef;
}

sub save {
    my (
        $self,

        #@type Entity::User
        $user
    ) = @_;
    my $sth = $self->{_dbh}->prepare('INSERT INTO users(name) VALUES (?)');
    $sth->execute( $user->getName );

    return $self->{_dbh}->last_insert_id();
}

1;
