package Service::Journal;

use warnings;
use strict;
use JSON;
use HTTP::Request;
use LWP::UserAgent;
use Data::Dumper;

sub new {
    my $class = shift;
    return bless(
        {
            _path => shift
        },
        $class
    );
}

sub save() {
    my ( $self, %data ) = @_;

    #TODO: send message
    my $json = encode_json \%data;

    #print "$json\n";
    my $req = HTTP::Request->new( 'POST', $self->{_path} );
    $req->header( 'Content-Type' => 'application/json' );
    $req->content($json);

    my $lwp      = LWP::UserAgent->new;
    my $response = $lwp->request($req);

    #print  Dumper($response->content);
}

1;
