package Service::Router;
use strict;
use warnings;
use Data::Dumper;

sub new {
    my ( $class, %params ) = @_;
    $params{_container} = {};

    return bless(
        {
            _container => {},
            _cgi       => $params{'cgi'},
            _path      => $params{'path'},
            _method    => $params{'method'},
        },
        $class
    );
}

sub add {
    my ( $self, $method, $url, $controller, $action ) = @_;
    unless ( $self->{_container}{$method} ) {
        $self->{_container}{$method} = ();
    }
    my %data =
      ( url => $url, 'controller' => $controller, 'action' => $action );
    push( @{ $self->{_container}{$method} }, \%data );

}

sub match() {
    my ($self) = @_;
    for my $route ( @{ $self->{_container}{ $self->{_method} } } ) {
        my $reg  = $$route{'url'};
        my $path = $self->{_path};
        $reg =~ s/\//#/g;
        $path =~ s/\//#/g;
        if ( my @result = $path =~ /^$reg$/ ) {
            $$route{'result'} = \@result;
            return $route;
        }
    }

    return 0;
}
1;
